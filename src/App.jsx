import React from 'react'
import Blocs from './components/Blocs'
import './App.css'
import { Tittle } from './components/Tittle'
import { Categories } from './components/Categories'
import { SousTitres } from './components/SousTitres'
import { MeilleurRecette } from './components/MeilleurRecettes'
import "./App.css";
import Header from "./Header";
import ArticlesFondas from "./components/Articles-fondas";

function App() {


  return (
    <React.Fragment>
      <Header />
      <Categories titre="Categorías" />
      <Tittle titre="Las favoritas" />

      <div className="container-blocs">
        <Blocs dotNumber="42" iconImage="./category-pizza.png" mealTitle="Pizza" mealPrice="Desde $60" />
        <Blocs dotNumber="35" iconImage="./category-sushi.png" mealTitle="Sushi" mealPrice="Desde $50" />
        <Blocs dotNumber="28" iconImage="./category-hamburger.png" mealTitle="Hamburguesas" mealPrice="Desde $50" />
        <Blocs dotNumber="23" iconImage="./category-veggie.png" mealTitle="Veggie" mealPrice="Desde $50" />
        <Blocs dotNumber="15" iconImage="./category-soup.png" mealTitle="Sopras" mealPrice="Desde $50" />
        <Blocs dotNumber="9" iconImage="./category-dessert.png" mealTitle="Postres" mealPrice="Desde $50" />
      </div>

      <Categories titre="Fonditas" />

      <Tittle titre=" Fondas cercanas" />

      <SousTitres titre="Estás son las fondas que se encuentran cerca!" />

      <div className="container-articles-fondas">

        <ArticlesFondas dishImage="./dish-dona-laura.jpeg" dishTitle="Doña Laura" dishDuration="20-30 min" dishCategorie="Fonditas" />
        <ArticlesFondas dishImage="./dish-rosa-cafe.jpeg" dishTitle="Rosa Cafe" dishDuration="~45 min" dishCategorie="Lonchería" />
        <ArticlesFondas dishImage="./dish-cottidiene.jpeg" dishTitle="Le cottidiene" dishDuration="15-20 min" dishCategorie="Sushi" />
        <ArticlesFondas dishImage="./dish-querreque.jpeg" dishTitle="Querreque" dishDuration="~50 min" dishCategorie="Veggies" />
      </div>

      <Categories titre="MENÚS" />

      <Tittle titre="Los mejores menús" />

      <SousTitres titre="Aquí están los mejores menús de la semana, y decide que vas a pedir " />

      <MeilleurRecette image="./public/menu-hot-caketerias.jpeg" categorie="Hot cakes" titre="Hot caketerías" texte="Incluye dos toppics" temps="20-30 min" coût="Desde $70" note={9.8} />
      <MeilleurRecette image="./public/menu-desayunos.jpeg" categorie="Continental" titre="Desayunos" texte="Incluye huevo y tostadas" temps="15-20 min" coût="Desde $50" note={9.8} />
      <MeilleurRecette image="./public/menu-otros-desayunos.jpeg" categorie="Hot cakes" titre="Otros desayunos" texte="Incluye dos jugos y tocinito" temps="~20 min" coût="Desde $50" note={9.8} />
      <MeilleurRecette image="./public/menu-pizzeria.jpeg" categorie="Pizza" titre="Pizzería" texte="Chingo de pizzas" temps="~45 min" coût="Desde $70" note={9.8} />
      <MeilleurRecette image="./public/menu-ensaladish.jpeg" categorie="Sushi" titre="Ensaladish" texte="Muchas frutas" temps="30-40 min" coût="Desde $70" note={9.8} />
      <MeilleurRecette image="./public/menu-pastish.jpeg" categorie="Pizza" titre="Pastish" texte="Una súper duper pasta" temps="10-20 min" coût="Desde $50" note={9.8} />

    </React.Fragment>






  );
}

export default App;
