import plateauxRepas from "./assets/header-hero.png"
import "./Header.css"
import NavBar from './Navbar'

function Header() {
    return (
        
       <header className="Bannière">
        <NavBar />
        <h2>Fonditas</h2>
        <h3>De tu fonda favorita</h3>
        <h1>La comida que ya conoces al mejor precio</h1>
        <img src={plateauxRepas} alt="Plateaux nourriture" />
        
       </header> 
    )

  }

export default Header;