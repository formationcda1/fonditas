import "./Navbar.css"


function NavBar() {
    return(
    <nav className="MenuNavigation">
        <ul>
            <li>Nosotros</li>
            <li>Fonditas</li>
            <li>Mapa</li>
            <li>Inscribirse</li>
        </ul>
    </nav>
    )
}

export default NavBar;