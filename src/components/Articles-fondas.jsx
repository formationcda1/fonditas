import './Articles-fondas.css'
const ArticlesFondas = function (props) {

    return (


        <div className="bloc-article-fondas">
            <img className="dish-image" src={props.dishImage} />
            <h4 className="dish-title">{props.dishTitle}</h4>
            
            <div className="price-categorie-container">
                <p className="dish-duration">{props.dishDuration}</p>
                <p className="dish-categorie">{props.dishCategorie}</p>
            </div>
        </div>

    )
}

export default ArticlesFondas
