import './Blocs.css'

const Blocs = function (props) {

    return (

        <div className="favorites-blocs-container">
            <div className="green-dot" >{props.dotNumber}</div>
            <div className='icon-container'><img className="icon-image" src={props.iconImage} /></div>
            <h4 className="meal-title">{props.mealTitle}</h4>
            <p className="meal-price">{props.mealPrice}</p>
        </div>

    )
}

export default Blocs
