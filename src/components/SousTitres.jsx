export function SousTitres(props) {
    return (
        <div className="sous--titres">
            <h3>{props.titre}</h3>
        </div>
    )

}