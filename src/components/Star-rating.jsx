import './Star-rating.css'
const StarRating = function (props) {

    return (


        <div className="star-rating-container">
            <img className="star-icon" src={props.starIcon} />
            <p className="rating">{props.noteRating}</p>
        </div>

    )
}

export default StarRating