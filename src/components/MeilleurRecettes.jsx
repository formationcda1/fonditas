import './MeilleurRecettes.css'
export function MeilleurRecette(props) {
    return (
        <article className="meilleur--recette">
            <img className="photo--recettes" src={props.image} alt="Photos menu" ></img>

            <div className='recette--container'>
                <p className='categorie--recette'>{props.categorie}</p>
                <h1 className='titre--recette'>{props.titre}</h1>
                <p className='texte--recette' >{props.texte}</p>
                <div className='info--container'>
                     <p className='temps--recette'>{props.temps}</p>
                <p className='cout--recette'>{props.coût}</p>
                </div>
               
            </div>

            <span className='note--recette'>{props.note} </span>
        </article>
    )
}