import React from 'react'
import Blocs from './components/Blocs'
import './App.css'
import { Tittle } from './components/Tittle'
import { Categories } from './components/Categories'
import { SousTitres } from './components/SousTitres'
import { MeilleurRecette } from './components/MeilleurRecettes'

function App() {




  return (
    <React.Fragment>

      <h2>Las Favoritas</h2>

      <Tittle titre="Las favoritas" />
      <Tittle titre=" Fondas cercanas" />
      <Tittle titre="Los mejores menús" />


      <Categories titre="Categorías" />
      <Categories titre="Fonditas" />
      <Categories titre="MENÚS" />


      <SousTitres titre="Estás son las fondas que se encuentran cerca!" />
      <SousTitres titre="Aquí están los mejores menús de la semana, y decide que vas a pedir " />

      <Blocs dotNumber="42" iconImage="./category-pizza.png" mealTitle="Pizza" mealPrice="Desde $60" />
      <Blocs dotNumber="35" iconImage="./category-sushi.png" mealTitle="Sushi" mealPrice="Desde $50" />
      <Blocs dotNumber="28" iconImage="./category-hamburger.png" mealTitle="Hamburguesas" mealPrice="Desde $50" />
      <Blocs dotNumber="23" iconImage="./category-veggie.png" mealTitle="Veggie" mealPrice="Desde $50" />
      <Blocs dotNumber="15" iconImage="./category-soup.png" mealTitle="Sopras" mealPrice="Desde $50" />
      <Blocs dotNumber="9" iconImage="./category-dessert.png" mealTitle="Postres" mealPrice="Desde $50" />

      
      <MeilleurRecette image="" categorie="Hot cakes" titre="Hot caketerías" texte="Incluye dos toppics" temps="20-30 min" coût="Desde $70" note={9.8}/>
      <MeilleurRecette image="" categorie="Continental" titre="Desayunos" texte="Incluye huevo y tostadas" temps="15-20 min" coût="Desde $50" note={9.8}/>
      <MeilleurRecette image="" categorie="Hot cakes" titre="Otros desayunos" texte="Incluye dos jugos y tocinito" temps="~20 min" coût="Desde $50" note={9.8}/>
      <MeilleurRecette image="" categorie="Pizza" titre="Pizzería" texte="Chingo de pizzas" temps="~45 min" coût="Desde $70" note={9.8}/>
      <MeilleurRecette image="" categorie="Sushi" titre="Ensaladish" texte="Muchas frutas" temps="30-40 min" coût="Desde $70" note={9.8}/>
      <MeilleurRecette image="" categorie="Pizza" titre="Pastish" texte="Una súper duper pasta" temps="10-20 min" coût="Desde $50" note={9.8}/>
    </React.Fragment>

  )
}

export default App
